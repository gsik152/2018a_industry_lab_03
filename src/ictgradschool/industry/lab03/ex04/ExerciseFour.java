package ictgradschool.industry.lab03.ex04;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {

    private void start() {

        // TODO Use other methods you create to implement this program's functionality.

        printNew(truncate(obtainAmount(),obtainDecimals()));
    }

    private String obtainAmount() {
        System.out.print("Enter an amount: ");
        String amount = Keyboard.readInput();
        return amount;
    }

    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard

    private int obtainDecimals() {
        System.out.print("Enter number of decimal places: ");
        int decimalPlaces = Integer.parseInt(Keyboard.readInput());
        return decimalPlaces;
    }

    // TODO Write a method which truncates the specified number to the specified number of DP's

    private String truncate(String amount, int decimals) {
        int decIndex = amount.indexOf('.');
        String newAmount = amount.substring(0, decIndex + 1) + amount.substring((decIndex + 1),(decIndex + decimals + 1));
        return  newAmount;
    }

    // TODO Write a method which prints the truncated amount

    private void printNew(String amount){
        System.out.println("The new amount is " + amount);
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
