package ictgradschool.industry.lab03.ex10;

public class Medals {

    // Constants
    public static final int GOLD_VALUE = 3;
    public static final int SILVER_VALUE = 2;
    public static final int BRONZE_VALUE = 1;

    // Instance variables:
    
    private String countryName; // name of country
    private int golds;   // number of gold medals
    private int silvers;  // number of silver medals
    private int bronzes;  // number of bronze medals
    
    
    public Medals(String countryName, int golds, int silvers, int bronzes) {
        this.countryName = countryName;
        this.golds = golds;
        this.silvers = silvers;
        this.bronzes = bronzes;
    }
    
    public String getCountryName() {
        return countryName;
    }
    
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
    
    public boolean isHigherThan(Medals other) {
        // Compare the other Medals object with this one.
        // The other Medals object is passed in as a parameter
        // return true if the current Medals object is higher (in terms of medals value)
        //  than the other.
        int medalValue = golds * GOLD_VALUE + silvers * SILVER_VALUE + bronzes * BRONZE_VALUE;

        int otherMedalValue = other.golds * GOLD_VALUE
                + other.silvers * SILVER_VALUE
                + other.bronzes * BRONZE_VALUE;

        return medalValue > otherMedalValue;
    }
    
    public boolean hasMoreGoldMedalsThan(Medals other) {
        if (this.golds > other.golds) {
            return true;
        }
        return false;
    }
    
    
    public String toString() {
        String newString = this.countryName + " has " + this.golds + " gold medals, " + this.silvers + " silver medals, " + this.bronzes + " bronze medals";
        return newString;
    }

}

