package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    public int obtainBounds(String prompt) {
        System.out.println(prompt + ":");
        String userString = Keyboard.readInput();
        int userIn = Integer.parseInt(userString);
        return  userIn;
    }

    public int obtainNumber(int lowerBound, int upperBound) {
        int num = (int) (Math.random() * ((upperBound - lowerBound) + 1) + lowerBound);
        return num;
    }

    public int findMin(int num1, int num2, int num3) {
        int a = Math.min(num1,num2);
        int b = Math.min(num2,num3);
        int minNum = Math.min(a,b);
        return minNum;
    }

    private void start() {
        int lowerBound = obtainBounds("Enter lower bound");
        int upperBound = obtainBounds("Enter upper bound");
        int number1 = obtainNumber(lowerBound,upperBound);
        int number2 = obtainNumber(lowerBound,upperBound);
        int number3 = obtainNumber(lowerBound,upperBound);
        int minNum = findMin(number1, number2, number3);
        System.out.println("The numbers are " + number1 + ", " + number2 + ", " + number3);
        System.out.println("The smallest number is " + minNum );

    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}